const Store = require('electron-store');
const _store = new Store();
const store: Record<string, any> = {};
['get', 'set', 'has', 'delete', 'clear', 'onDidChange', 'onDidAnyChange'].forEach(action => {
    store[action] = (...args: any[]) => _store[action](...args);
});
['size', 'store', 'path'].forEach(key => {
    store[key] = () => _store[key];
})
export default store
