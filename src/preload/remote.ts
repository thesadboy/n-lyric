import {dialog, app} from "@electron/remote";
import {OpenDialogSyncOptions} from 'electron';

function showOpenDialogSync(options: OpenDialogSyncOptions): (string[]) | (undefined) {
    return dialog.showOpenDialogSync(options)
}

// @ts-ignore
const lyric = app.lyric;
export default {
    showOpenDialogSync,
    lyric,
}
