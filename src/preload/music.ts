import mm from 'music-metadata';
import {IAudioMetadata} from "music-metadata/lib/type";

const getMusicData = async (path: string): Promise<IAudioMetadata> => {
    return mm.parseFile(path);
}
export default {
    getMusicData
}
