import fs from 'fs';

const isDirectory = (filePath: string): boolean => {
    return fs.statSync(filePath).isDirectory();
}

export default {
    isDirectory
}
