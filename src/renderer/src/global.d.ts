import {OpenDialogSyncOptions} from "electron";
import {IAudioMetadata} from "music-metadata/lib/type";

export {}
interface Remote {
    showOpenDialogSync(options: OpenDialogSyncOptions): (string[]) | (undefined),
}

interface File {
    isDirectory(filePath: string): boolean
}

declare global {
    interface Window {
        node: {
            fs: typeof import('fs'),
            path: typeof import('path')
        },
        music: {
            getMusicData(path: string): Promise<IAudioMetadata>
        },
        $file: File,
        store: import('electron-store')
        remote: Remote,
        ipcRenderer: import('electron').IpcRenderer
        removeLoading: () => void
    }
}
