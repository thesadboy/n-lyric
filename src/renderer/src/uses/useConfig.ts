import {reactive, toRaw} from "vue";

interface Setting {
    pathType: 'same' | 'custom',
    path: string
}

const store = window.store;
const storeSetting: Setting = (store.get('setting') || {pathType: 'same', path: ''}) as Setting
const setting = reactive(storeSetting);

const save = () => {
    store.set('setting', toRaw(setting));
}


export {
    setting,
    save
}
