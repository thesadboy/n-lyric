import axios from 'axios';

const callback = (data: any) => data;
const getLyric = async (songmid: any) => {
    let {data: lyricData} = await axios.get('https://c.y.qq.com/lyric/fcgi-bin/fcg_query_lyric_new.fcg', {
        headers: {
            'referer': 'y.qq.com/portal/player.html'
        },
        params: {
            songmid: songmid,
            callback: 'callback',
            g_tk: 5381
        }
    });
    lyricData = eval(lyricData).lyric;
    if (!lyricData) return null;
    return Buffer.from(lyricData, 'base64').toString();
}

const queryList = async (file: any) => {
    const {title = '', artist = ''} = file.meta.common;
    let {data: infoData} = await axios.get('https://c.y.qq.com/soso/fcgi-bin/client_search_cp', {
        params: {
            w: `${title} ${artist}`,
        }
    });
    const {data: {song: {list}}} = eval(infoData);
    let exact = null;
    if (list.length) {
        exact = list.find((item: any) => item.songname === title) || list[0];
    }
    return {
        list,
        exact
    };
}

export default {
    getLyric,
    callback,
    queryList
}
